import java.io.*;import java.net.*;import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ahsanqureshi
 */

@WebServlet(name = "CountryValid",
            urlPatterns = {"/CountryValid"})

public class CountryValid extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        response.setContentType("text/plain;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        String countryName = request.getParameter("country");
        
        if (countryName=="") {
            out.print("Please select a country/region.");
        } 
        else {
            out.print("ok");
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    processRequest(request, response);
    }
}