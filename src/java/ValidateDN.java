import java.io.*;import java.net.*;import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ahsanqureshi
 */

@WebServlet(name = "ValidateDN",
            urlPatterns = {"/ValidateDN"})

public class ValidateDN extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        response.setContentType("text/plain;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();
        String dispName = request.getParameter("alias");
        
        if (dispName.length() < 3 || dispName.length() > 31) {
            out.print("Invalid display name.");
        } 
        else {
            out.print("ok");
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    processRequest(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    processRequest(request, response);
    }
}